# publish.py
import os
import sys
from jinja2 import Environment, FileSystemLoader

# Define the drafts and staging directories
DRAFTS_DIRECTORY = 'drafts'
PUBLISHED_DIRECTORY = 'published'
PUBLIC_DIRECTORY = os.path.join('..', 'public', 'blog')

TEMPLATES_DIRECTORY = 'templates'
PAGE_TEMPLATE_NAME = 'page.html'

def main():
    
    #The script needs at least one argument with the path to an html file to function
    if len(sys.argv) < 2 or sys.argv[1].split('.')[-1] != 'html':
        print("Usage: python publish.py <filename>.html")
        sys.exit(1)
    
    script_first_argument = sys.argv[1]
    filename = script_first_argument

    draft_path = os.path.join(DRAFTS_DIRECTORY, filename)
    published_path = os.path.join(PUBLISHED_DIRECTORY, filename)
    public_path_blog = os.path.join(PUBLIC_DIRECTORY, filename)
    public_path_index = os.path.join(PUBLIC_DIRECTORY, 'index.html')

    templates_environment = Environment(loader=FileSystemLoader(TEMPLATES_DIRECTORY))

    blog_draft_content = get_draft_content(draft_path)
    # Remove file extension from filename
    blog_title = os.path.splitext(filename)[0].title()
    blog_html = render_html(
        templates_environment, PAGE_TEMPLATE_NAME, blog_title, blog_draft_content)
    # Copy final draft to published folder
    publish_html(published_path, blog_html)
    # Copy final draft to public folder
    publish_html(public_path_blog, blog_html)
    
    index_content = render_index(templates_environment, 'blog-index.html')
    index_html = render_html(
        templates_environment, PAGE_TEMPLATE_NAME, 'All blogs', index_content)
    # Copy final draft to public folder
    publish_html(public_path_index, index_html)

    print(f"Published '{draft_path}' to '{published_path}'")


def get_draft_content(draft_path):
    if not os.path.exists(draft_path):
        print(f"No such file: '{draft_path}'")
        sys.exit(1)
    with open(draft_path, 'r') as file:
        return file.read()


def render_html(templates_environment, template_name, title, draft_content):
    template = templates_environment.get_template(template_name)
    return template.render(title=title, content=draft_content)


def publish_html(published_path, final_content):
    # Ensure the directory exists
    os.makedirs(os.path.dirname(published_path), exist_ok=True)
    with open(published_path, 'w') as file:
        file.write(final_content)


def render_index(templates_environment, template_name):
    file_list = [f.split('.')[0] for f in os.listdir(PUBLISHED_DIRECTORY) if os.path.isfile(
        os.path.join(PUBLISHED_DIRECTORY, f))]
    template = templates_environment.get_template(template_name)
    return template.render(file_list=file_list)

# Call the main() function only if the script is executed as the main module
if __name__ == '__main__':
    main()
